modEUrn = {
	modname = minetest.get_current_modname(),
	modpath = minetest.get_modpath(minetest.get_current_modname()),
	urlTabela = minetest.get_worldpath().."/e-urn.db", --Extensao '.tbl' ou '.db'
}

dofile(modEUrn.modpath.."/translate.lua") -- <== Antes de 'api.lua'!
dofile(modEUrn.modpath.."/api.lua")
dofile(modEUrn.modpath.."/formspecs.lua")
dofile(modEUrn.modpath.."/commands.lua")
dofile(modEUrn.modpath.."/item_eurn.lua")

modEUrn.doLoad()

minetest.log('action',"["..modEUrn.modname:upper().."] Loaded!")
